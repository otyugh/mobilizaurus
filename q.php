<?php
ini_set('display_errors', 'On');error_reporting(E_ALL | E_STRICT);
require('./geohash.php');
use Sk\Geohash\Geohash;

function dump($var){
	echo '<p>[TIME:'.(time()-$_SERVER["REQUEST_TIME"]).']</p><pre>'.print_r($var,true).'</pre>';
}

//options import
if(!isset($_GET['mode'])){
	$mode='json';
}else{
	$mode=$_GET['mode'];
}


if(!isset($_GET['online']) || $online=($_GET['online']==1)){
	if(isset($_GET['gps'])&&!empty($_GET['gps'])){
		#custom location
		$gps=explode(';',$_GET['gps']);
		$g = new Geohash();
		$geohash=$g->encode($gps[1],$gps[0],9);
		unset($g);

		if(!isset($_GET['radius'])||empty($_GET['radius'])){$radius='20';}elseif($_GET['radius']>=1000){die("Radius MAX is 1000");}else{$radius=$_GET['radius'];}
		if(!isset($_GET['locality'])){$locality='ANONYMOUS LOCATION';}else{$locality=$_GET['locality'];}
	}else{
		#everywhere
		$geohash=null;
		$radius=null;
		if(!isset($_GET['locality'])){$locality='EVERYWHERE';}else{$locality=$_GET['locality'];}
	}

	$online=false;#'null'
}else{
	#online
	$online=true;#'"ONLINE"'
	$geohash=null;
	$radius=null;
}

//optionnal & backup value
if(isset($_GET['btag'])){$btag=$_GET['btag'];}else{$btag='';}
if(isset($_GET['wtag'])){$wtag=$_GET['wtag'];}else{$wtag='';}
if(isset($_GET['borganizer'])&& !empty($_GET['borganizer'])){$borganizer=explode(';',$_GET['borganizer']);}else{$borganizer=array();}
if(isset($_GET['worganizer'])&& !empty($_GET['worganizer'])){$worganizer=explode(';',$_GET['worganizer']);}else{$worganizer=array();}
if(isset($_GET['dpast'])&&!empty($_GET['dpast'])){$ostart=$_GET['dpast'];}else{$ostart=0;}
if(isset($_GET['dfuture'])&&!empty($_GET['dfuture'])){$end=$_GET['dfuture'];}else{$end='null';}
const MAX_ITEM=5000;
if(isset($_GET['rmax'])&&!empty($_GET['rmax'])&&$_GET['rmax']<MAX_ITEM){
	$limit=$_GET['rmax'];
}else{
	$limit=MAX_ITEM;
}
$epoch=time();

$fingerprint='./cache/'.md5($geohash.$radius.$end.$ostart.$online);
$start='"'.date('Y-m-d\TH:m:s\Z',$epoch-86400*$ostart).'"';
if($end!='null'){$end='"'.date('Y-m-d\TH:m:s\Z',time()+86400*$end).'"';}

//TODO:
$target="INTERNAL";//$target="GLOBAL";
$descr='Description'; //description of the announcer ?
$sort='MATCH_DESC'; //what are the other option ?
$self='https://foo.bar'; //does it even matter ?

if(!file_exists('./cache/')){mkdir('./cache/', 0700);}
if(file_exists($fingerprint) && (filemtime($fingerprint)+3600)>time()){
	$s=file_get_contents($fingerprint);
}else{
	$a = curl_init();
	curl_setopt($a, CURLOPT_RETURNTRANSFER, "true");
	curl_setopt($a, CURLOPT_URL, "https://mobilizon.fr/api");
	curl_setopt($a, CURLOPT_HTTPHEADER, array("content-type:application/json"));
	curl_setopt($a, CURLOPT_POSTFIELDS, '{"operationName":"SearchEventsAndGroups","variables":{"term":"","location":'.(($geohash==null)?'null':'"'.$geohash.'"').',"beginsOn":'.$start.',"endsOn":'.$end.',"radius":'.(($radius==null)?'null':$radius).',"eventPage":1,"groupPage":1,"limit":'.MAX_ITEM.',"type":'.(($online)?'"ONLINE"':'null').',"categoryOneOf":[],"statusOneOf":["CONFIRMED"],"languageOneOf":[],"searchTarget":"'.$target.'","sortByEvents":"'.$sort.'","sortByGroups":"'.$sort.'","boostLanguages":["fr","en"]},"query":"query SearchEventsAndGroups($location: String, $radius: Float, $tags: String, $term: String, $type: EventType, $categoryOneOf: [String], $statusOneOf: [EventStatus], $languageOneOf: [String], $searchTarget: SearchTarget, $beginsOn: DateTime, $endsOn: DateTime, $bbox: String, $zoom: Int, $eventPage: Int, $groupPage: Int, $limit: Int, $sortByEvents: SearchEventSortOptions, $sortByGroups: SearchGroupSortOptions, $boostLanguages: [String]) {\n  searchEvents(\n    location: $location\n    radius: $radius\n    tags: $tags\n    term: $term\n    type: $type\n    categoryOneOf: $categoryOneOf\n    statusOneOf: $statusOneOf\n    languageOneOf: $languageOneOf\n    searchTarget: $searchTarget\n    beginsOn: $beginsOn\n    endsOn: $endsOn\n    bbox: $bbox\n    zoom: $zoom\n    page: $eventPage\n    limit: $limit\n    sortBy: $sortByEvents\n    boostLanguages: $boostLanguages\n  ) {\n    total\n    elements {\n      id\n      title\n      uuid\n      beginsOn\n      picture {\n        id\n        url\n        __typename\n      }\n      url\n      status\n      tags {\n        ...TagFragment\n        __typename\n      }\n      physicalAddress {\n        ...AdressFragment\n        __typename\n      }\n      organizerActor {\n        ...ActorFragment\n        __typename\n      }\n      attributedTo {\n        ...ActorFragment\n        __typename\n      }\n      participantStats {\n        participant\n        __typename\n      }\n      options {\n        isOnline\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  searchGroups(\n    term: $term\n    location: $location\n    radius: $radius\n    languageOneOf: $languageOneOf\n    searchTarget: $searchTarget\n    bbox: $bbox\n    zoom: $zoom\n    page: $groupPage\n    limit: $limit\n    sortBy: $sortByGroups\n    boostLanguages: $boostLanguages\n  ) {\n    total\n    elements {\n      __typename\n      id\n      avatar {\n        id\n        url\n        __typename\n      }\n      type\n      preferredUsername\n      name\n      domain\n      summary\n      url\n      ...GroupResultFragment\n      banner {\n        id\n        url\n        __typename\n      }\n      followersCount\n      membersCount\n      physicalAddress {\n        ...AdressFragment\n        __typename\n      }\n    }\n    __typename\n  }\n}\n\nfragment TagFragment on Tag {\n  id\n  slug\n  title\n  __typename\n}\n\nfragment AdressFragment on Address {\n  id\n  description\n  geom\n  street\n  locality\n  postalCode\n  region\n  country\n  type\n  url\n  originId\n  timezone\n  pictureInfo {\n    url\n    author {\n      name\n      url\n      __typename\n    }\n    source {\n      name\n      url\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment GroupResultFragment on GroupSearchResult {\n  id\n  avatar {\n    id\n    url\n    __typename\n  }\n  type\n  preferredUsername\n  name\n  domain\n  summary\n  url\n  __typename\n}\n\nfragment ActorFragment on Actor {\n  id\n  avatar {\n    id\n    url\n    __typename\n  }\n  type\n  preferredUsername\n  name\n  domain\n  summary\n  url\n  __typename\n}"}');
	$s=curl_exec($a);
	$rn=curl_getinfo($a)['http_code'];
	if($rn!=200){
		dump($s);
		die('Error sending (err: '.$rn.')');
	}
	file_put_contents($fingerprint,$s);
	unset($a);
}
$r=json_decode($s)->data->searchEvents->elements;

//small fix for convenience later
$rcount=count($r);
for($i=$rcount-1;$i>=0;$i--){
	//date format to EPOCH
	$r[$i]->beginsOn=strtotime($r[$i]->beginsOn);
	$r[$i]->title=htmlentities($r[$i]->title);
	//remove events in excess
	if($i<$rcount-$limit){
		unset($r[$i]);
	}else{
		//TODO:bug with online event mixed in physical events
		if(($online && isset($r[$i]->physicalAddress->geom)) || (!$online && !isset($r[$i]->physicalAddress->geom))){
			//MAKE A LOOOOT OF POSITIVE BOTH WAY
			unset($r[$i]);
		}else{
			//TODO:bug with one past event sometimes reported here https://framacolibri.org/t/bug-evenement-dans-le-passe-non-demande/20531
			if($r[$i]->beginsOn < $epoch - $ostart*86400){
//				dump($r[$i]);

				unset($r[$i]);
			}else{
				//TODO:dunno the difference but feels meaningless so let's unite them for now
				if(empty($r[$i]->attributedTo)){$r[$i]->attributedTo=$r[$i]->organizerActor;}
				//filter by organizer
				if(in_array($r[$i]->attributedTo->name,$borganizer) || (count($worganizer)!=0 && !in_array($r[$i]->attributedTo->name,$worganizer))){
					unset($r[$i]);
				}else{
					//filter by tag
					$r[$i]->tag='';
					$w=empty($wtag);
					foreach($r[$i]->tags as $t){
						$r[$i]->tag.='#'.$t->title.' ';
						if(!empty($btag) && in_array($t->title,explode(';',$btag))){
							unset($r[$i]);
							break;
						}
						if(!empty($wtag) && in_array($t->title,explode(';',$wtag))){
							$w=true;
							break;
						}
					}
					if(!$w){unset($r[$i]);}
				}
			}
		}
	}
}

if($mode=='debug'){
	echo 'curl \'https://mobilizon.fr/api\' --compressed -X POST -H \'Content-Type: application/json\'  --data-raw \'{"operationName":"SearchEventsAndGroups","variables":{"term":"","location":'.$geohash.',"beginsOn":'.$start.',"endsOn":'.$end.',"radius":'.$radius.',"eventPage":1,"groupPage":1,"limit":'.MAX_ITEM.',"type":'.(($online)?'"ONLINE"':'null').',"categoryOneOf":[],"statusOneOf":["CONFIRMED"],"languageOneOf":[],"searchTarget":"'.$target.'","sortByEvents":"'.$sort.'","sortByGroups":"'.$sort.'","boostLanguages":["fr","en"]},"query":"query SearchEventsAndGroups($location: String, $radius: Float, $tags: String, $term: String, $type: EventType, $categoryOneOf: [String], $statusOneOf: [EventStatus], $languageOneOf: [String], $searchTarget: SearchTarget, $beginsOn: DateTime, $endsOn: DateTime, $bbox: String, $zoom: Int, $eventPage: Int, $groupPage: Int, $limit: Int, $sortByEvents: SearchEventSortOptions, $sortByGroups: SearchGroupSortOptions, $boostLanguages: [String]) {\n  searchEvents(\n    location: $location\n    radius: $radius\n    tags: $tags\n    term: $term\n    type: $type\n    categoryOneOf: $categoryOneOf\n    statusOneOf: $statusOneOf\n    languageOneOf: $languageOneOf\n    searchTarget: $searchTarget\n    beginsOn: $beginsOn\n    endsOn: $endsOn\n    bbox: $bbox\n    zoom: $zoom\n    page: $eventPage\n    limit: $limit\n    sortBy: $sortByEvents\n    boostLanguages: $boostLanguages\n  ) {\n    total\n    elements {\n      id\n      title\n      uuid\n      beginsOn\n      picture {\n        id\n        url\n        __typename\n      }\n      url\n      status\n      tags {\n        ...TagFragment\n        __typename\n      }\n      physicalAddress {\n        ...AdressFragment\n        __typename\n      }\n      organizerActor {\n        ...ActorFragment\n        __typename\n      }\n      attributedTo {\n        ...ActorFragment\n        __typename\n      }\n      participantStats {\n        participant\n        __typename\n      }\n      options {\n        isOnline\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  searchGroups(\n    term: $term\n    location: $location\n    radius: $radius\n    languageOneOf: $languageOneOf\n    searchTarget: $searchTarget\n    bbox: $bbox\n    zoom: $zoom\n    page: $groupPage\n    limit: $limit\n    sortBy: $sortByGroups\n    boostLanguages: $boostLanguages\n  ) {\n    total\n    elements {\n      __typename\n      id\n      avatar {\n        id\n        url\n        __typename\n      }\n      type\n      preferredUsername\n      name\n      domain\n      summary\n      url\n      ...GroupResultFragment\n      banner {\n        id\n        url\n        __typename\n      }\n      followersCount\n      membersCount\n      physicalAddress {\n        ...AdressFragment\n        __typename\n      }\n    }\n    __typename\n  }\n}\n\nfragment TagFragment on Tag {\n  id\n  slug\n  title\n  __typename\n}\n\nfragment AdressFragment on Address {\n  id\n  description\n  geom\n  street\n  locality\n  postalCode\n  region\n  country\n  type\n  url\n  originId\n  timezone\n  pictureInfo {\n    url\n    author {\n      name\n      url\n      __typename\n    }\n    source {\n      name\n      url\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment GroupResultFragment on GroupSearchResult {\n  id\n  avatar {\n    id\n    url\n    __typename\n  }\n  type\n  preferredUsername\n  name\n  domain\n  summary\n  url\n  __typename\n}\n\nfragment ActorFragment on Actor {\n  id\n  avatar {\n    id\n    url\n    __typename\n  }\n  type\n  preferredUsername\n  name\n  domain\n  summary\n  url\n  __typename\n}"}\'';
	die();
}

//Reset array

$rcount=count($r);
$r=array_values($r);
if($rcount==0){die("No result");}

//RSS
if($mode=='rss'){
header('Content-Type: application/rss+xml; charset=utf-8');
echo '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
	<atom:link rel="self" href="'.$self.'" type="application/rss+xml" />
	<title>Évènements '.(($online)?'en ligne':'à ~'.$radius.'km de '.$locality).'</title>
	<link>'.$self.'</link>
	<description>'.$descr.'</description>
';


foreach($r as $el) {
	echo '
	<item>
		<title>'.str_replace(array('&', '<'), array('&#x26;', '&#x3C;'), $el->title).'</title>
		<category>Évènement Mobilizon</category>
		<description><![CDATA[
		'.(($online)?'':'<p>Lieu : '.$el->physicalAddress->description.' '.((empty($el->physicalAddress->locality)||$el->physicalAddress->locality==$el->physicalAddress->description)?'':'('.$el->physicalAddress->locality.')').'</p>').'
		<p>Date : '.date("d/m/Y \à H\hi",$el->beginsOn).'</p>
		<p>Par : '.((empty($el->attributedTo))?'<a href="'.$el->organizerActor->url.'">'.$el->organizerActor->name.'</a>':'<a href="'.$el->attributedTo->url.'">'.$el->attributedTo->name.'</a>').'</p>
		]]></description>
		<link>'.$el->url.'</link>
		<guid>'.$el->url.'</guid>
		<pubDate>'.date(DATE_RFC2822,$el->beginsOn).'</pubDate>
	</item>';
//		<enclosure url="'.WWW_ROOT.DATA.$file['path'].'" length="'.$file['size'].'" type="'.$file['type'].'" />
}
echo '
</channel>
</rss>';
}elseif($mode=='agenda'){
?>
<!DOCTYPE html>
<html>
<head>
<style>
table {width: 100%; table-layout: fixed;}
tr {vertical-align: top; background-color: #A1C3E7;}
ul {list-style-type: none; margin: 0; padding: 0;}
li {overflow: hidden; white-space: nowrap; text-overflow: ellipsis;}
li:hover {white-space: unset;}
.present { background-color: #FFE0C7 }
.empty { background-color: white }
.past { background-color: gray }
</style>
</head>
<body>
<?php


//today
$dy=date('j');
//current month
$cm=date('n');
//current year
$y=date('o');
//days in current month
$dm=cal_days_in_month(CAL_GREGORIAN, $cm, $y);

//month counter
$months=array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
$mi=0;
$j=$rcount-1;

while($mi<12){
	echo '<h2>'.$months[$cm-1].'</h2><table>';
	$i=1;
	while($i<=35){
		$s='';
		$e='';
		if((-1+$i%7)==0){
			$s.='<tr>';
		}
		$s.='<td';
		$e.='</td>'.$e;

		if($i>$dm){
			$s.=' class="empty"';
		}else{
			if($mi==0){
				if($dy==$i){
					$s.=' class="present">'.$i;
				}elseif($dy>$i){
					$s.=' class="past">'.$i;
				}else{
					$s.='>'.$i;
				}	
			}else{
				$s.='>'.$i;
			}
		}		
		$s.='<ul>';
		while($j>=0&&$i==date('j',$r[$j]->beginsOn)&&$cm==date('n',$r[$j]->beginsOn)){
			$s.='<li><a href="'.$r[$j]->url.'">'.(($online)?'':'<b>'.$r[$j]->physicalAddress->description.'</b>').' : '.$r[$j]->title.'</a> par '.$r[$j]->attributedTo->name.'</li>';
			$j--;
		}
		echo $s.$e;
		$i++;
		$e='</ul>';
		if($j<0){$mi=12; $dm=0;}
	}
	echo '</table>';
	$mi++;
	$cm++;
	if($cm>12){$y++;$cm=1;}
	$dm=cal_days_in_month(CAL_GREGORIAN, $cm, $y);
}
echo '</body>';
}elseif($mode=='list'){
	echo '<h1>'.$rcount.'</h1><style>.past{opacity:0.4;} li{margin-bottom:10px;}</style>
<ul>';
	$inpast=($r[$rcount-1]->beginsOn < $epoch);	
	for($i=$rcount-1;$i>=0;$i--){
		if($inpast && $r[$i]->beginsOn > $epoch){echo "<hr>"; $inpast=false;}
		echo (($inpast)?'<li class="past">':'<li>').'<a href="'.$r[$i]->url.'">'.$r[$i]->title.'</a><br>'.date("d/m/Y \à H\hi",$r[$i]->beginsOn).' <b>'.((empty($r[$i]->physicalAddress->locality))?'en ligne':'à '.$r[$i]->physicalAddress->locality).'</b> '.((count($worganizer)!=1)?' par '.$r[$i]->attributedTo->name:'').'<br><small>'.$r[$i]->tag.'</small></li>';
	}
	echo '</ul>';
}elseif($mode=='map' && !$online){
	if(!isset($gps)){
		$zoom=6;
		$gps=array(2,46); //middle of france
	}else{
		//$radius=10*pow(2,12-$zoom);
		$zoom=(13-log($radius/5,2));
	}
	echo '<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <style>
      html, body {
        height: 100%;
        padding: 0;
        margin: 0;
      }
      #map {
        width: 100%;
        height: 100%;
      }
    </style>
  </head>
  <body>
	<div id="map"></div>
	<script>
	var map = L.map(\'map\').setView({lon: '.$gps[0].', lat: '.$gps[1].'}, '.$zoom.');
	L.tileLayer(\'https://tile.openstreetmap.org/{z}/{x}/{y}.png\', {maxZoom: 19,attribution: \'&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>\'}).addTo(map);
	L.control.scale({imperial: true, metric: true}).addTo(map);
	var greyIcon = new L.Icon({
		iconUrl: \'img/marker-icon-2x-grey.png\',
		shadowUrl: \'img/marker-shadow.png\',
		iconSize: [25, 41],
		iconAnchor: [12, 41],
		popupAnchor: [1, -34],
		shadowSize: [41, 41]
	});
';
	if(isset($radius)){echo '	var circle = L.circle(['.$gps[1].', '.$gps[0].'], '.($radius*1000).', {color: \'green\',fillColor: \'red\',fillOpacity: 0.00}).addTo(map);';}
	$i=$rcount;
	$inpast=($r[$i-1]->beginsOn < $epoch);
	while($i>0){
		$i--;
		if($inpast && $r[$i]->beginsOn > $epoch){$inpast=false;}
		$a=explode(';',$r[$i]->physicalAddress->geom);
		echo 'L.marker({lon: '.$a[0].', lat: '.$a[1].'}'.(($inpast)?', {icon: greyIcon}':'').').bindPopup("'.'<a href=\"'.$r[$i]->url.'\">'.$r[$i]->title.'</a><br>'.date("d/m/Y \à H\hi",$r[$i]->beginsOn).' <b>'.((empty($r[$i]->physicalAddress->locality))?'':'à '.$r[$i]->physicalAddress->locality).'</b> '.((count($worganizer)!=1)?' par '.$r[$i]->attributedTo->name:'').'<br><small>'.$r[$i]->tag.'</small>'.'").addTo(map);
';
	}
echo '
    </script>
  </body>
</html>';
}elseif($mode=='json'){
	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($r);
}else{
	echo "Mode invalide";
}
?>