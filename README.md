# Mobilizaurus

Simple fronted to Mobilizon : provided a location and a radius it provides the list of event available in that area.

It uses the Mobilizon API to find GPS point from a location ; you can also provide some directly.

You can check what it looks like [here](https://mobilizaurus.arzinfo.eu.org/)

## Provides views as :
* Map (using Leaflet)
* RSS
* HTML presentation : List / Agenda

## Filtering options
* Filter result by organizer and by tag ; either by whitlisting (all must contain X) or by blacklisting (everything but X).
* Limit the number of result by number (the one coming the latest will be ignored)
* Limit the results by date (you can display past event, limit the future event displayed to a number of days)

## Additional features
* Cache results for 24h.

## Todo
* Handle when mobilizon instance curl fails / incorrect
* Notification by mail & sms
* Change the instance querried ?
* Interface language
* Inscription / Paying entry / Open event
* Cleanup (ahaha)
* Option to remove headers as to implement the result in a web page
* why can't I tape into the federated thing ?