<?php
ini_set('display_errors', 'On');error_reporting(E_ALL | E_STRICT);

$error='';

if(isset($_GET['locality'])){
	$locality=$_GET['locality'];
}else{
	$locality='';
}

if(isset($_GET['wtag'])){
	$wtag=$_GET['wtag'];
}else{
	$wtag='';
}

if(isset($_GET['btag'])){
	$btag=$_GET['btag'];
}else{
	$btag='';
}

if(isset($_GET['worganizer'])){
	$worganizer=$_GET['worganizer'];
}else{
	$worganizer='';
}

if(isset($_GET['borganizer'])){
	$borganizer=$_GET['borganizer'];
}else{
	$borganizer='';
}

if(isset($_GET['rmax'])){
	$rmax=$_GET['rmax'];
}else{
	$rmax='';
}

if(isset($_GET['radius'])){
	$radius=$_GET['radius'];
}else{
	$radius='';
}

if(isset($_GET['gps'])){
	$a=explode(';',$_GET['gps']);
	if(count($a)==3){
		$locality=$a[2];
		$gps=$a[0].';'.$a[1];
		unset($a);
	}else{
		$gps=$_GET['gps'];
	}
}else{
	$gps='';
}
$valide=false;

if(isset($_GET['dpast'])){
	$dpast=$_GET['dpast'];
}else{
	$dpast='';
}

if(isset($_GET['dfuture'])){
	$dfuture=$_GET['dfuture'];
}else{
	$dfuture='';
}

if(isset($_GET['online']) && $_GET['online']==0){
	$online=true;
	$locality='';
	$gps='';
	$radius='';
}else{
	$online=false;
}

//guess with mobilizon
if(empty($gps) && !empty($locality)){
	function str_2_localities($str){
		$a = curl_init();
		curl_setopt($a, CURLOPT_RETURNTRANSFER, "true");
		curl_setopt($a, CURLOPT_URL, "https://mobilizon.fr/api");
		curl_setopt($a, CURLOPT_HTTPHEADER, array("content-type:application/json"));
		curl_setopt($a, CURLOPT_POSTFIELDS, '{"operationName":"SearchAddress","variables":{"query":"'.$str.'","locale":"fr","type":"ADMINISTRATIVE"},"query":"query SearchAddress($query: String!, $locale: String, $type: AddressSearchType) {\n  searchAddress(query: $query, locale: $locale, type: $type) {\n    ...AdressFragment\n    __typename\n  }\n}\n\nfragment AdressFragment on Address {\n  id\n  description\n  geom\n  street\n  locality\n  postalCode\n  region\n  country\n  type\n  url\n  originId\n  timezone\n  pictureInfo {\n    url\n    author {\n      name\n      url\n      __typename\n    }\n    source {\n      name\n      url\n      __typename\n    }\n    __typename\n  }\n  __typename\n}"}');
		return json_decode(curl_exec($a))->data->searchAddress;
	}
	$localities=str_2_localities($locality);
	if(count($localities)==0){
		$guessgps='Aucun lieu trouvé pour "'.$locality.'"';
	}elseif(count($localities)==1){
		$gps=$localities[0]->geom;
		$locality=$localities[0]->description;
	}else{
		$guessgps=count($localities).' lieux trouvés pour "'.$locality.'"';
	}
}else{
	//header("Location: q.php$_SERVER[REQUEST_URI]",TRUE,307);
	//die();
}

?>
<!DOCTYPE html>
<html>
<head>
	<style>div>input{width:auto;}button,input{box-sizing:content-box; width:170px;}div>button{width: 10em;height: 5em;}legend{font-weight: bold;}label{display: inline-block;}div{background: #ffe8d4;border: 1px solid #f69d3c;padding: 5px;border-radius: 5px; margin:1em}</style>
<head>

<body>
<h1>Mobilizaurus</h1>
<em>Ne ratez plus les prochaines rencontres, ateliers, bouffes, révolutions... !</em>
<hr>
<?php
if(!empty($error)){
	echo "<div>$error</div>";
}
?>
<form>
	<fieldset><legend>Filtre primaire</legend>
		<input type="hidden" name="online" value="<?=(($online)?'0':'1')?>">
		<button type="submit" name="online" value="1" formnovalidate><?=(($online)?'Géographique':'<b>Géographique</b>')?></button>
		<button type="submit" name="online" value="0" formnovalidate><?=((!$online)?'En ligne':'<b>En ligne</b>')?></button><br>
<?php
if(!$online){
?>
<?php
	echo '<hr>';
	if(isset($guessgps)){
		echo "<div><p>$guessgps</p>";
		if(!empty($locality)&&empty($gps)&&count($localities)>1){
			foreach($localities as $el) {
				echo '		<input type="radio" name="gps" value="'.$el->geom.';'.$el->description.'" id="'.$el->geom.'" required><label for="'.$el->geom.'">'.$el->description.' ('.$el->region.((empty($el->region))?'':', ').$el->country.')</label>';
			}
		}
		echo '</div>';
		echo '		<label>Rayon (km)<input type="number" name="radius" value="'.$radius.'" placeholder="30" required></label><br>';
	}else{
?>
		<label>Nom d'un lieu<br><input type="text" name="locality" value="<?=$locality?>" placeholder="Quimperlé, Bretagne"></label>
		<button type="submit">==chercher==></button>
		<label>Point GPS<br><input type="text" name="gps" value="<?=$gps?>" placeholder="-3.554068;47.8707"></label><br>
		<label>Rayon (km)<br><input type="number" name="radius" value="<?=$radius?>" placeholder="30"></label><br>
		<small>Pour des résultats "mondiaux" laissez tout vide.</small>
<?php
	}
}
?>
	</fieldset>
	<fieldset><legend>Filtres secondaires</legend>
			<label>Passé visible (en jours)<br><input type="text" name="dpast" value="<?=$dpast?>" placeholder="0"></label>
			<label>Avenir visible (en jours)<br><input type="text" name="dfuture" value="<?=$dfuture?>" placeholder="90"></label>
			<label>Nombre de résultats max<br><input type="text" name="rmax" value="<?=$rmax?>" placeholder="500"></label><br>
			<label>Tag requis<br><input type="text" name="wtag" value="<?=$wtag?>" placeholder="#Manifestation"></label>
			<label>Organisateur requis<br><input type="text" name="worganizer" value="<?=$worganizer?>" placeholder="Arborepom"></label>
			<br><small>Système de WhiteList : les évènements ne remplissant pas au moins une des condition(s) seront exclues.</small><br>
			<label>Tag exclus<br><input type="text" name="btag" value="<?=$btag?>" placeholder="#ManifPourTous"></label>
			<label>Organisateur exclus<br><input type="text" name="borganizer" value="<?=$borganizer?>" placeholder="FNSEA;MEDEF"></label>
			<br><small>Système de BlackList : les évènements remplissant une des conditions seront exclues.</small>
 	</fieldset>
	<hr>
	<h2>Résultats</h2>
	<em>À mettre en marque page ou à intégrer dans un site !</em>
 	<div>
		<button type="submit" name="mode" value="list" formaction="q.php">Liste</button>
		<button type="submit" name="mode" value="agenda" formaction="q.php">Agenda</button>
		<button type="submit" name="mode" value="rss" formaction="q.php">Flux RSS</button>
		<?php if(!$online){echo '<button type="submit" name="mode" value="map" formaction="q.php">Carte</button>';}?>
	</div>

</form> 
<hr>

<small><p>Les requêtes sont mises en cache pour 1h pour épargner l'API de <a href="https://mobilizon.fr">Mobilizon.fr</a></p>
<p>Source du site <a href="https://salsa.debian.org/otyugh/mobilizaurus">là</a>.</p></small>
</body>
</html>